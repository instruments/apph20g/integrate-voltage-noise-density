"""
APPH20G FFT analyzer CSV data post-processing

    * calculate RMS voltage noise [VRMS]
      by integrating measured voltage noise density [V/sqrt(HZ)] samples
    * display data with calculated results

Author: Alexander Dietrich (alexander.dietrich@psi.ch)
Date: 18.01.2021
"""


# import modules

import csv
import numpy
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from scipy import integrate


# filename

file = 'CVD_FE_noise_measurement.csv'


# open CSV measurement data

with open(file, "r") as csv_header:
    csv_reader_1 = list(csv.reader(csv_header, delimiter=','))
del csv_reader_1[12:]


# print device information

print(str(csv_reader_1[0][0]) + "\n" + str(csv_reader_1[1][0]))


# print trace list and select trace to process

trace_list = csv_reader_1[2]
trace_list_1st = str(trace_list[0])
trace_list[0] = trace_list_1st[10:]
print("\nNumber of traces:", len(trace_list))
for i in range(len(trace_list)):
    print(str(i+1) + ":" + trace_list[i])
print("\nSelect trace to plot:")
trace_num = int(input())


# define frequency range for integration

with open(file, "r") as csv_data:
    csv_reader_2 = list(csv.reader(csv_data, delimiter=';'))
del csv_reader_2[:12]
csv_data_size = int(csv_reader_1[3][1])
csv_f_row = (trace_num * 2) - 2
f_min = csv_reader_2[csv_f_row][0]
f_max = csv_reader_2[csv_f_row][(csv_data_size - 1)]
print("\nf_min [Hz]: ", f_min)
print("f_max [Hz]: ", f_max)
"""
print("\nEnter f_lo [Hz] in range f_min <= f_low < f_max:")
f_l = int(input())
print("\nEnter f_hi [Hz] in range f_min < f_high <= f_max:")
f_u = int(input())
"""


# create data array and calculate integral 

rawdata = numpy.array(csv_reader_2[csv_f_row:csv_f_row+2], dtype=numpy.float)
xdata = rawdata[0,:]
ydata = rawdata[1,:]
v_rms = numpy.sqrt(integrate.trapz(numpy.square(ydata), xdata))
v_rms /= 1e3 
v_rms_cum = numpy.sqrt(integrate.cumtrapz(numpy.square(ydata), xdata))
v_rms_cum /= 1e3
v_rms_cum = numpy.insert(v_rms_cum, 0, 0.0)
print("\n" + str(trace_list[trace_num-1]))
print("V_rms = % 5.3f uV" %(v_rms))


# plot measured sample data and calculated cumulative results

fig, ax1 = plt.subplots()
ax2 = ax1.twinx()
ax1.plot(xdata, ydata, label="Voltage Noise Density", color='b')
ax2.plot(xdata, v_rms_cum, label="Cumulative Integral", color='r')
ax1.set_xlabel("f [Hz]")
ax1.set_xscale("log")
ax1.set_ylabel("Vn nV/sqrt(Hz)", color='b')
ax2.set_ylabel("Vn uV_rms", color='r')
#ax2.set_yticks(numpy.linspace(ax2.get_yticks()[0], ax2.get_yticks()[-1], len(ax1.get_yticks())))
ax1.grid(which='both', axis='both')
ax2.grid(which='both', axis='both')
ax1.set_ylim(0,3500)
ax2.set_ylim(0,350)
ax1.yaxis.set_major_locator(MaxNLocator(5))
ax1.yaxis.set_minor_locator(MaxNLocator(10)) 
ax2.yaxis.set_major_locator(MaxNLocator(5))
ax2.yaxis.set_minor_locator(MaxNLocator(10))
"""
plt.figure(1, dpi=120)
plt.xscale("log")
plt.yscale("linear")
plt.xlabel("f[Hz]")
plt.ylabel("Vn[nV/sqrt(Hz)]")
plt.grid("both", "both")
plt.plot(xdata, ydata, label="Voltage Noise Density")
plt.plot(xdata, v_rms_cum, label="Cumulative Integral")
"""

plt.title(str(trace_list[trace_num-1]))
#plt.legend()
plt.show()