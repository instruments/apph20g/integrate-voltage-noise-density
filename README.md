# integrate-voltage-noise-density

Script to integrate and plot voltage noise density [nV/sqrt(Hz)] of exported CSV data from FFT analyzer.
The integrated output is plotted as cumulative and absolute noise [uVrms].  

Dependencies:
* python3
* matplotlib
* numpy
* scipy